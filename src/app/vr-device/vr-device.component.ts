import { Component, OnInit, Input} from '@angular/core';
import { Device } from '../device/device';
import { Light } from '../device/light';
import { Temprature } from '../device/temprature';

@Component({
    selector: 'app-vr-device',
    templateUrl: './vr-device.component.html',
    styleUrls: ['./vr-device.component.css']
})
export class VrDeviceComponent implements OnInit {
    
    @Input() private device: Device;

    constructor() {
         
    }

    ngOnInit() {
    }

}
