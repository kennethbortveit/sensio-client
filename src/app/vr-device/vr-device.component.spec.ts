import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VrDeviceComponent } from './vr-device.component';

describe('VrDeviceComponent', () => {
  let component: VrDeviceComponent;
  let fixture: ComponentFixture<VrDeviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VrDeviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VrDeviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
