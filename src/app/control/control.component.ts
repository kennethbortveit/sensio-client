import { Component, OnInit } from '@angular/core';
import { DeviceService } from '../services/device.service';
import { Device } from '../device/device';
import { Light } from '../device/light';
import { Temprature } from '../device/temprature';
import { ViewChild } from '@angular/core';
import * as io from "socket.io-client";

@Component({
  selector: 'app-control',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.css']
})
export class ControlComponent implements OnInit {

    @ViewChild('deviceList') private deviceList;

    private deviceService: DeviceService;
    private inputDeviceName: string;
    private inputDeviceRoom: string;
    private inputDeviceTypeOf: string;
    private socket = io('http://localhost:4000');

    constructor(deviceService: DeviceService) {
        this.deviceService = deviceService; 
        this.inputDeviceTypeOf = "device";
    }

    ngOnInit() {
        this.socket.on('update', (msg) => {
            console.log(msg);
            this.update();
        });
        this.socket.on('update-single-device', (data) => {
            this.deviceList.updateSingleDevice(data);
        });
    }

    public addDevice(): void{
        if(this.inputDeviceTypeOf == 'light') {
            this.deviceService.addDevice(new Light(this.inputDeviceName, this.inputDeviceRoom, this.inputDeviceTypeOf)); 
        } else if (this.inputDeviceTypeOf == 'temprature') {
            this.deviceService.addDevice(new Temprature(this.inputDeviceName, this.inputDeviceRoom, this.inputDeviceTypeOf)); 
        } else {
            this.deviceService.addDevice(new Device(this.inputDeviceName, this.inputDeviceRoom, this.inputDeviceTypeOf));
        }

        this.resetDeviceInput();

        this.socket.emit('update-devices', {msg: 'Socket update.'});
    }
    
    public update(): void {
        this.deviceList.fetchDevices();
    }

    private resetDeviceInput(): void {
        this.inputDeviceName = '';
        this.inputDeviceRoom = '';
        this.inputDeviceTypeOf = "device";
    }
}
