import { Device } from './device';

export class Temprature extends Device {
    
    public targetTemp: number;
    private currentTemp: number;

    constructor(name: string, room: string, typeOfDevice: string) {
        super(name, room, typeOfDevice);
        this.targetTemp = 20;
        this.currentTemp = 20;
    }

    public getTargetTemp(): number {
        return this.targetTemp;
    }
    
    public setTargetTemp(degrees: number): void {
        this.targetTemp = degrees;
    }
    
    public getCurrentTemp(): number {
        return this.currentTemp;
    }

    public setCurrentTemp(degrees: number): void {
        this.currentTemp = degrees;
    }
}
