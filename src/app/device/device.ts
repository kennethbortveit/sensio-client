export class Device {

    private id: string;
    private name: string;
    private power: string;
    private room: string;
    private imgUrl: string;
    private typeOfDevice: string;
    public status: string;

    constructor(name: string, room: string, typeOfDevice: string) {
        this.name = name;
        this.power = 'off';
        this.room = room;
        this.setImgUrl(typeOfDevice);
        this.typeOfDevice = typeOfDevice;
        this.status = "";
    }

    public getName(): string {
        return this.name;
    }

    public setName(name: string): void {
        this.name = name;
    }

    public getPower(): string {
        return this.power;
    }

    public setPower(power: string): void {
        this.power = power;
    }

    public getRoom(): string {
        return this.room;
    }

    public setRoom(room: string): void {
        this.room = room;
    }

    public getImgUrl(): string {
        return this.imgUrl;
    }

    public setImgUrl(typeOfDevice: string): void {
        if(typeOfDevice == 'light') {
            this.imgUrl = '/assets/img/light-bulb.png';
        } else if (typeOfDevice == 'temprature') {
            this.imgUrl = '/assets/img/temprature.png';
        } else {
            this.imgUrl = '/assets/img/device-symbol.png';
        }
    }

    public setId(id: string) {
        this.id = id;
    }

    public getId(): string {
        return this.id;
    }

    public getTypeOfDevice(): string {
        return this.typeOfDevice;
    }

    public setTypeOfDevice(typeOfDevice: string): void {
        this.typeOfDevice = typeOfDevice;
    }

}

export const TESTDEVICES: Device[] = [
    new Device('Spotter', 'Stue', 'light'),
    new Device('Kaffetrakter', 'Kjøkken', ''),
    new Device('Taklampe', 'Soverom Kenneth', 'light'),
    new Device('Tempratur', 'Inngangsparti', 'temprature'),
    new Device('Tempratur', 'Soverom 2', 'temprature')
];
