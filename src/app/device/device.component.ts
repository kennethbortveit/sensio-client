import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Device, TESTDEVICES } from '../device/device';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DeviceService } from '../services/device.service';
import * as io from "socket.io-client";


@Component({
    selector: 'app-device',
    templateUrl: './device.component.html',
    styleUrls: ['./device.component.css']
})
export class DeviceComponent implements OnInit {


    closeResult: string;
    private modalService: NgbModal;
    private deviceService: DeviceService;

    @Input() private socket;
    @Input() private device: Device;
    @Output() private removeButtonClicked: EventEmitter<string>;
    @Output() private updateButtonClicked: EventEmitter<Device>;

    constructor(modalService: NgbModal, deviceService: DeviceService) {
        this.deviceService = deviceService;
        this.modalService = modalService; 
        this.removeButtonClicked = new EventEmitter<string>();
        this.updateButtonClicked = new EventEmitter<Device>();
    }

    ngOnInit() {
    }
    
    open(content){
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    private removeDevice(id: string) {
        this.removeButtonClicked.emit(id);
    }
    
    private update(device: Device) {
        this.deviceService.updateDevice(device).subscribe((msg) => {
            this.device.status = "Updated.";
        });
        this.socket.emit('update-device', JSON.stringify(this.device));
    }

    private updateSingleDevice() {
        this.deviceService.updateDevice(this.device).subscribe((msg) => {
            this.device.status = "Updated.";
        });
        this.updateButtonClicked.emit(this.device);
    }
    
}
