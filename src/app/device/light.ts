import { Device } from './device';

export class Light extends Device {
    public dim: number;
    
    constructor(name: string, room: string, typeOfDevice: string) {
        super(name, room, typeOfDevice);
        this.dim = 50;
    }

    public setDim(dim: number): void {
        this.dim = dim;
    }

    public getDim(): number {
        return this.dim;
    }

}
