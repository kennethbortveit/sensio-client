import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Device, TESTDEVICES } from '../device/device';
import { Light } from '../device/light';
import { Temprature } from '../device/temprature';
import { Room } from '../vr-room/room';

@Injectable()

export class DeviceService {

    private http: Http;
    private deviceUrl: string;
    private devicesUrl: string;
    private updateDeviceUrl: string;
    private roomsUrl: string;

    constructor(http: Http) {
        this.http = http; 
        this.deviceUrl = 'http://localhost:3000/device';
        this.devicesUrl = 'http://localhost:3000/devices';
        this.updateDeviceUrl = 'http://localhost:3000/update-device';
        this.roomsUrl = 'http://localhost:3000/rooms';
    }
    
    public fetchDevices(): Observable<Device[]> {
        return this.http.get(this.devicesUrl).map((response: Response) => {
            let deviceJson = response.json();
            let deviceObjects = [];

            for(let device of deviceJson) {
                if(device.typeOfDevice == 'light') {
                    var light = new Light(device.name, device.room, device.typeOfDevice);
                    light.setId(device._id);
                    light.setDim(device.dim);
                    light.setPower(device.power);
                    deviceObjects.push(light);
                } else if (device.typeOfDevice == 'temprature') {
                    var temprature = new Temprature(device.name, device.room, device.typeOfDevice);
                    temprature.setId(device._id);
                    temprature.setCurrentTemp(device.currentTemp);
                    temprature.setTargetTemp(device.targetTemp);
                    temprature.setPower(device.power);
                    deviceObjects.push(temprature);
                } else {
                    var other = new Device(device.name, device.room, device.typeOfDevice);
                    other.setId(device._id);
                    other.setPower(device.power);
                    deviceObjects.push(other);
                }
            }
            return deviceObjects;
        });
    }

    public addDevice(device: Device): void {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({headers: headers});

        this.http.post(this.deviceUrl, JSON.stringify(device), options).map( (response: Response) => {
            return response.json();
        }).subscribe( (msg) => {
            console.log(msg);   
        });
    }

    public removeDevice(id: string): void {
        let headers = new Headers({'Content-Type': 'json/application'});
        this.http.delete(`${this.deviceUrl}/${id}`, {headers: headers})
            .subscribe((res: Response)=>{
                console.log(res); 
            });
    }

    public updateDevice(device: Device): Observable<string> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({headers: headers});

        return this.http.post(this.updateDeviceUrl, JSON.stringify(device), options).map( (response: Response) => {
            return JSON.stringify(response.json());
        });
    }

    public fetchRooms(): Observable<Room[]> {
        return this.http.get(this.roomsUrl).map( (response: Response) => {
            let roomsJson = response.json();
            let roomsObjects = [];

            for (let room of roomsJson) {
                roomsObjects.push(new Room(room));         
            }
            
            return roomsObjects; 
        });
    }
}
