import { Component, OnInit, Input } from '@angular/core';
import { Device } from '../device/device';
import { DeviceService } from '../services/device.service';
import { Light } from '../device/light';
import { Temprature } from '../device/temprature';
import * as io from "socket.io-client";


@Component({
  selector: 'app-device-list',
  templateUrl: './device-list.component.html',
  styleUrls: ['./device-list.component.css']
})
export class DeviceListComponent implements OnInit {
    @Input() private socket;
    private devices: Device[];
    private deviceService: DeviceService;

    constructor(deviceService: DeviceService) {
        this.deviceService = deviceService;
    }

    ngOnInit() {
        this.fetchDevices();
    }

    public fetchDevices(): void {
        this.deviceService.fetchDevices().subscribe((devices) => {
            this.devices = devices;
        });
    }
    
    public removeDevice(id: string): void {
        this.deviceService.removeDevice(id); 
        this.socket.emit('delete-device', {msg: 'A device has been deleted.'});
    }

    public updateDevice(device: Device): void {
        this.deviceService.updateDevice(device);
        this.socket.emit('update-device', JSON.stringify(device));
    }

    public updateSingleDevice(device: Object): void {

        let y: Device = this.findDevice(device['id']);

        y.setName(device['name']);
        y.setRoom(device['room']);
        y.setTypeOfDevice(device['typeOfDevice']); 
        y.setImgUrl(device['typeOfDevice']);
        y.setPower(device['power']);

        
        if(device['typeOfDevice'] == 'temprature') {
            let temprature: Temprature = <Temprature>y;
            temprature.setCurrentTemp(device['currentTemp']);
            temprature.setTargetTemp(device['targetTemp']);

        } else if (device['typeOfDevice'] == 'light') {
            let light: Light = <Light>y;
            light.setDim(device['dim']);
            
        } else {
            console.log('This is just a device.');
        }
    }

    public findDevice(id: string): Device {

        let x: Device = null;

        for (let device of this.devices) {
            if (device.getId() == id) {
                x = device;
            }
        }
        return x;
    }
}
