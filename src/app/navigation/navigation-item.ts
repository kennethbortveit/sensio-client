export class NavigationItem {
    private title: string;
    private link: string;

    constructor(title: string, link: string) {
        this.title = title;
        this.link = link;
    }

    public getTitle(): string {
        return this.title;
    }

    public getLink(): string {
        return this.link;
    }
}

export const ITEMS: NavigationItem[] = [
    new NavigationItem('Hjem', 'home'),
    new NavigationItem('Kontroll', 'control'),
    new NavigationItem('Virtuelt hjem', 'virtual-home')
];
