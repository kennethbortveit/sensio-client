import { Component, OnInit } from '@angular/core';
import { NavigationItem, ITEMS } from './navigation-item';

@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
    
    private items: NavigationItem[]; 

    constructor() {
        this.items = ITEMS; 
    }

    ngOnInit() {
    }

}
