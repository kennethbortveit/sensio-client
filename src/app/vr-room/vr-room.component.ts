import { Component, OnInit, Input } from '@angular/core';
import { Device } from '../device/device';
import { Light } from '../device/light';
import { Temprature } from '../device/temprature';
import { Room } from '../vr-room/room';

@Component({
  selector: 'app-vr-room',
  templateUrl: './vr-room.component.html',
  styleUrls: ['./vr-room.component.css']
})
export class VrRoomComponent implements OnInit {
    @Input() room: Room;

    constructor() { }

    ngOnInit() {
    }
    
}
