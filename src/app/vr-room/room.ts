import { Device } from '../device/device';
import { Light } from '../device/light';
import { Temprature } from '../device/temprature';

export class Room {
    
    private name: string;
    private devices: Device[];

    constructor(name: string) {
        this.name = name;
        this.devices = [];
    }

    public getName(): string {
        return this.name;
    }

    public getDevices(): Device[] {
        return this.devices;
    }

    public addDevice(device: Device) {
        this.devices.push(device);
    }
}
