import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VrRoomComponent } from './vr-room.component';

describe('VrRoomComponent', () => {
  let component: VrRoomComponent;
  let fixture: ComponentFixture<VrRoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VrRoomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VrRoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
