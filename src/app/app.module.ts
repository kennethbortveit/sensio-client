import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import 'hammerjs';

import { DeviceService } from './services/device.service';

import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { HomeComponent } from './home/home.component';
import { ControlComponent } from './control/control.component';
import { DeviceComponent } from './device/device.component';
import { DeviceListComponent } from './device-list/device-list.component';
import { VirtualHomeComponent } from './virtual-home/virtual-home.component';
import { VrDeviceComponent } from './vr-device/vr-device.component';
import { VrRoomComponent } from './vr-room/vr-room.component';

@NgModule({
    declarations: [
        AppComponent,
        NavigationComponent,
        HomeComponent,
        ControlComponent,
        DeviceComponent,
        DeviceListComponent,
        VirtualHomeComponent,
        VrDeviceComponent,
        VrRoomComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        NgbModule.forRoot(),
        RouterModule.forRoot([
            {path: '', redirectTo: '/home', pathMatch: 'full'},
            {path: 'home', component: HomeComponent},
            {path: 'control', component: ControlComponent},
            {path: 'virtual-home', component: VirtualHomeComponent}
        ]),
        MaterialModule

    ],
    providers: [
        DeviceService 
    ],
    bootstrap: [AppComponent]
    })
    export class AppModule { }
