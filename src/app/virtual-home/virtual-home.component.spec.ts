import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VirtualHomeComponent } from './virtual-home.component';

describe('VirtualHomeComponent', () => {
  let component: VirtualHomeComponent;
  let fixture: ComponentFixture<VirtualHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VirtualHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VirtualHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
