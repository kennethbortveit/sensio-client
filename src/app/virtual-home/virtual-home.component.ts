import { Component, OnInit } from '@angular/core';
import { Device } from '../device/device';
import { Light } from '../device/light';
import { Temprature } from '../device/temprature';
import { DeviceService } from '../services/device.service';
import { Room } from '../vr-room/room';
import * as io from "socket.io-client";



@Component({
    selector: 'app-virtual-home',
    templateUrl: './virtual-home.component.html',
    styleUrls: ['./virtual-home.component.css']
})


export class VirtualHomeComponent implements OnInit {

    private deviceService: DeviceService;

    private devices: Device[];
    private rooms: Room[];
    private socket = io('http://localhost:4000');

    constructor(deviceService: DeviceService) {
        this.deviceService = deviceService; 
        this.devices = [];
        this.rooms = [];
        
    }

    ngOnInit() {
        this.update();

        this.socket.on('update-single-device', (data) => {
            let device = this.findDevice(data['id']); 
            device.setPower(data['power']);
            if(data['typeOfDevice'] == 'light') {
                let light = <Light>device;
                light.setDim(data['dim']);
            }
            if (data['typeOfDevice'] == 'temprature') {
                let temprature = <Temprature>device;
                temprature.setCurrentTemp(data['currentTemp']);
                temprature.setTargetTemp(data['targetTemp']);
            }
        });

        this.socket.on('update', (data) => {
            this.update();
        });

    }

    private findDevice(id: string): Device {
        let device: Device = null;
        for(let x of this.devices) {
            if(x.getId() == id) {
                device = x;
            }
        }
        return device;
    }

    private update(): void {
        this.deviceService.fetchDevices().subscribe((devices) => {
            this.devices = devices;
            
            this.deviceService.fetchRooms().subscribe((rooms) => {
                this.rooms = rooms;
                for(let device of this.devices) {
                    for(let room of this.rooms) {
                        if(device.getRoom() == room.getName()){
                            room.addDevice(device);
                        }
                    }
                }
            });
        });

    }

}
